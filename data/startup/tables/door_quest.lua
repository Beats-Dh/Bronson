--[[

Look README.md for look the reserved action/unique

]]
QuestDoorAction = {
	-- The queens of the banshee door
	[Storage.Quest.TheQueenOfTheBanshees.FirstSeal] = {
		itemId = 1777,
		itemPos = {{x = 1197, y = 757, z = 7}}
	}
}

QuestDoorUnique = {
	--Dawnport
	-- Vocation doors
	-- Sorcerer
	[22001] = {
		itemId = 12195,
		itemPos = {x = 32055, y = 31885, z = 6}
	},
	-- Druid
	[22002] = {
		itemId = 7040,
		itemPos = {x = 32073, y = 31885, z = 6}
	},
	-- Paladin
	[22003] = {
		itemId = 6898,
		itemPos = {x = 32059, y = 31885, z = 6}
	},
	-- Knight
	[22004] = {
		itemId = 9279,
		itemPos = {x = 32069, y = 31885, z = 6}
	}
}
