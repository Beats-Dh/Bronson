local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end

	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid

-- CONFIG --	
local gold = {
p1 = 10000, -- Pre�o em Gold 1 ao 100
p2 = 20000, -- Pre�o em Gold 101 ao 150
p3 = 30000, -- Pre�o em Gold 151 ao 200
p4 = 50000, -- Pre�o em Gold 201 ao 250
p5 = 60000, -- Pre�o em Gold 251 ao 299
p6 = 70000 -- Pre�o em Gold 300 em diante
}
-- CONFIG --	
local player = Player(cid)
	if msgcontains(msg, 'valor') then
		doPlayerPopupFYI(cid, "Pre�o ben��o:\n\nLeve 1 ao 100: "..gold.p1.." gp\nLeve 101 ao 150: "..gold.p2.." gp\nLeve 151 ao 200: "..gold.p3.." gp\nLeve 201 ao 250: "..gold.p4.." gp\nLeve 251 ao 299: "..gold.p5.." gp\nLeve 300 em diante: "..gold.p6.." gp")
	end
	if msgcontains(msg, 'bless') then
		if getPlayerBlessing(cid, 7) then
			selfSay('Voc� j� foi aben�oado pelo Senhor!', cid)
		return end
		
			if getPlayerLevel(cid) >= 1 and getPlayerLevel(cid) <= 100 then
				if getPlayerMoney(cid) >= gold.p1 then
					doPlayerRemoveMoney(cid, gold.p1)
for i = 1, 7 do
			if not player:hasBlessing(i) then
				player:addBlessing(i, 1)
			end
		end
					selfSay('Parab�ns {'..getPlayerName(cid)..'}, agora voc� tem a ben��o!', cid)
					doSendMagicEffect(getPlayerPosition(cid), 29)
				else
					selfSay('Nosso Deus precisa muito de dinheiro, e voc� n�o tem o suficiente! Volte quando tiver!', cid)
				end

			elseif getPlayerLevel(cid) >= 101 and getPlayerLevel(cid) <= 150 then
				if getPlayerMoney(cid) >= gold.p2 then
					doPlayerRemoveMoney(cid, gold.p2)
for i = 1, 7 do
			if not player:hasBlessing(i) then
				player:addBlessing(i, 1)
			end
		end
					selfSay('Parab�ns {'..getPlayerName(cid)..'}, agora voc� tem a ben��o!', cid)
					doSendMagicEffect(getPlayerPosition(cid), 29)
				else
					selfSay('Nosso Deus precisa muito de dinheiro, e voc� n�o tem o suficiente! Volte quando tiver!', cid)
				end

			elseif getPlayerLevel(cid) >= 151 and getPlayerLevel(cid) <= 200 then
				if getPlayerMoney(cid) >= gold.p3 then
					doPlayerRemoveMoney(cid, gold.p3)
for i = 1, 7 do
			if not player:hasBlessing(i) then
				player:addBlessing(i, 1)
			end
		end
					selfSay('Parab�ns {'..getPlayerName(cid)..'}, agora voc� tem a ben��o!', cid)
					doSendMagicEffect(getPlayerPosition(cid), 29)
				else
					selfSay('Nosso Deus precisa muito de dinheiro, e voc� n�o tem o suficiente! Volte quando tiver!', cid)
				end

			elseif getPlayerLevel(cid) >= 201 and getPlayerLevel(cid) <= 250 then
				if getPlayerMoney(cid) >= gold.p4 then
					doPlayerRemoveMoney(cid, gold.p4)
for i = 1, 7 do
			if not player:hasBlessing(i) then
				player:addBlessing(i, 1)
			end
		end
					selfSay('Parab�ns {'..getPlayerName(cid)..'}, agora voc� tem a ben��o!', cid)
					doSendMagicEffect(getPlayerPosition(cid), 29)
				else
					selfSay('Nosso Deus precisa muito de dinheiro, e voc� n�o tem o suficiente! Volte quando tiver!', cid)
				end

			elseif getPlayerLevel(cid) >= 251 and getPlayerLevel(cid) <= 299 then
				if getPlayerMoney(cid) >= gold.p5 then
					doPlayerRemoveMoney(cid, gold.p5)
player:addBlessing(1)
for i = 1, 7 do
			if not player:hasBlessing(i) then
				player:addBlessing(i, 1)
			end
		end
					selfSay('Parab�ns {'..getPlayerName(cid)..'}, agora voc� tem a ben��o!', cid)
					doSendMagicEffect(getPlayerPosition(cid), 29)
				else
					selfSay('Nosso Deus precisa muito de dinheiro, e voc� n�o tem o suficiente! Volte quando tiver!', cid)
				end

			elseif getPlayerLevel(cid) >= 300 then
				if getPlayerMoney(cid) >= gold.p6 then
					doPlayerRemoveMoney(cid, gold.p6)
for i = 1, 7 do
			if not player:hasBlessing(i) then
				player:addBlessing(i, 1)
			end
		end
					selfSay('Parab�ns {'..getPlayerName(cid)..'}, agora voc� tem a ben��o!', cid)
					doSendMagicEffect(getPlayerPosition(cid), 29)
				else
					selfSay('Nosso Deus precisa muito de dinheiro, e voc� n�o tem o suficiente! Volte quando tiver!', cid)
				end				
			end
	end
return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())