local bless = TalkAction("!bless")

local config = {
premium = true, -- se precisa ser premium account (true or false)
pz = true -- se precisa estar em pz (true or false)
}

function bless.onSay(player, words, param)
	if player:getPremiumDays() <= 0 and config.premium == true then
	player:sendTextMessage(MESSAGE_STATUS_WARNING, "Apenas contas vip tem esse recurso.")
	player:getPosition():sendMagicEffect(CONST_ME_POFF)
	return false
end

if not Tile(player:getPosition()):hasFlag(TILESTATE_PROTECTIONZONE) and config.pz == true then
     player:sendCancelMessage("Voce tem que esta em uma zona de protecao.")
  return false
end

	return Blessings.BuyAllBlesses(player)
end

bless:register()
